import 'dart:convert';
import 'dart:io' show Platform;

import 'package:http/http.dart' as http;
import 'package:flutter_snow_blower/src/utils/utils.dart';

class PubService {
  Future<String> getPackage(String pack, String version) async {
    String url = _getUrlPackages() + "/$pack";

    if (version.isNotEmpty) {
      url += "/versions/$version";
    }

    var response = await http.get(url);

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      var map = version.isEmpty ? json['latest']['pubspec'] : json['pubspec'];
      return map['version'];
    } else {
      throw Exception("error");
    }
  }

  String _getUrlPackages() {
    String baseUrl = 'https://pub.dev';

    Map<String, String> envVars = Platform.environment;
    final String baseUrlEnv = envVars['PUB_HOSTED_URL'];

    if (baseUrlEnv != null && validateUrl(baseUrlEnv)) {
      baseUrl = baseUrlEnv;
    }

    return "$baseUrl/api/packages";
  }
}
