import 'dart:io';

import 'package:args/command_runner.dart';
import 'package:dart_console/dart_console.dart';
import 'package:flutter_snow_blower/src/command/generate_command.dart';
import 'package:flutter_snow_blower/src/templates/templates.dart' as templates;
import 'package:flutter_snow_blower/src/utils/file_utils.dart';
import 'package:flutter_snow_blower/src/utils/object_generate.dart';
import 'package:flutter_snow_blower/src/utils/output_utils.dart' as output;
import 'package:flutter_snow_blower/src/utils/utils.dart';

import 'package:flutter_snow_blower/src/modules/install.dart';
import 'package:tuple/tuple.dart';

Map<String, int> providerSystemOptions = {
  'flutter_clean_architecture': 0,
  'flutter_modular': 1,
};

Map<String, int> stateManagementOptions = {'mobx': 0};

int stateCLIOptions(String title, List<String> options) {
  stdin.echoMode = false;
  stdin.lineMode = false;
  var console = Console();
  var isRunning = true;
  var selected = 0;

  while (isRunning) {
    print('\x1B[2J\x1B[0;0H');
    output.title('Flutter Snow Blower CLI Interative\n');
    output.warn(title);
    for (var i = 0; i < options.length; i++) {
      if (selected == i) {
        print(output.green(options[i]));
      } else {
        print(output.white(options[i]));
      }
    }

    print('\nUse ↑↓ (keyboard arrows)');
    print('Press \'q\' to quit.');

    var key = console.readKey();

    if (key.controlChar == ControlCharacter.arrowDown) {
      if (selected < options.length - 1) {
        selected++;
      }
    } else if (key.controlChar == ControlCharacter.arrowUp) {
      if (selected > 0) {
        selected--;
      }
    } else if (key.controlChar == ControlCharacter.enter) {
      isRunning = false;
      return selected;
    } else if (key.char == 'q') {
      return -1;
    } else {}
  }
  return -1;
}

Function cleanOrModular([int selected, String directory]) {
  selected ??= stateCLIOptions('What Architecture do you want to use?',
      ['Clean architecture (default)', 'Modular']);

  if (selected == -1) {
    exit(1);
  }

  return () async {
    await removeAllPackages(directory);
    if (selected == 0) {
      output.msg("Instaling Clean architecture...");
      await install(["build_runner", "injectable_generator"], true, directory: directory);
      await install(["get_it", "injectable"], false, directory: directory);
    } else if (selected == 1) {
      output.msg("Instaling Modular...");
    } else {
      exit(1);
    }
  };
}

Function selectStateManagement([int selected, String directory]) {
  // selected ??= stateCLIOptions('Choose a state manager', ['mobx (default)']);

  // if (selected == -1) {
  //   exit(1);
  // }

  return () async {
    output.title("Starting a new project with Mobx");
    await install(["mobx", 'flutter_mobx'], false, directory: directory);
    await install(["build_runner", "mobx_codegen"], true, directory: directory);
  };
}

Future isContinue(Directory dir, [int selected]) async {
  if (await dir.exists()) {
    if (dir.listSync().isNotEmpty) {
      selected ??= stateCLIOptions(
          'This command will delete everything inside the \"lib /\" and \"test\" folders.',
          [
            'No',
            'Yes',
          ]);
    }
    if (selected == 1) {
      output.msg('Removing lib folder');
      await dir.delete(recursive: true);
    } else {
      output.error('The lib folder must be empty');
      exit(1);
    }
  }
}

Future<void> start({
  completeStart,
  bool force = false,
  Directory dir,
  Tuple2<Function, Function> tuple,
  String providerSystem,
  String stateManagement,
  bool useEnviroment = true,
}) async {
  dir ??= Directory('lib');
  tuple ??= Tuple2(cleanOrModular(providerSystemOptions[providerSystem]),
      selectStateManagement(stateManagementOptions[stateManagement]));
  await isContinue(dir, force ? 1 : null);
  await tuple.item1();
  await tuple.item2();

  var dirTest = Directory(dir.parent.path + '/test');
  if (await dirTest.exists()) {
    if (dirTest.listSync().isNotEmpty) {
      output.msg("Removing test folder");
      await dirTest.delete(recursive: true);
    }
  }

  var command =
      CommandRunner("slidy", "CLI package manager and template for Flutter.");
  command.addCommand(GenerateCommand());

  var package = await getNamePackage(dir.parent);

  _installPackages(package);

  _createMainFile(
    dir.path,
    package,
    useEnviroment: useEnviroment,
  );

  _createStyleFiles();

  _createSharedFiles(package, completeStart);

  _createVsCodeSettings(package);

  _createAndroidStudioSettings(package);

  _createAppWidgetAndModule(package, completeStart);

  await command.run(['generate', 'module', 'modules/home', '-c']);

  if (completeStart) {
    createStaticFile(
      libPath('modules/splash/splash_page.dart'),
      templates.pageSplashGenerator(
        ObjectGenerate(name: "Splash", packageName: package),
      ),
    );

    await command.run(['generate', 'module', 'modules/login', '-c']);
  }

  await command.run(['generate', 'controller', 'app']);

  output.msg("Project started! enjoy!");
}

void _createAppWidgetAndModule(String package, bool complete) {
  if (complete) {
    createStaticFile(
      libPath('app_module.dart'),
      templates.startAppModuleModularComplete(package),
    );
    createStaticFile(
      libPath('app_widget.dart'),
      templates.startAppWidgetComplete(package),
    );
  } else {
    createStaticFile(
      libPath('app_module.dart'),
      templates.startAppModuleModular(package),
    );

    createStaticFile(
      libPath('app_widget.dart'),
      templates.startAppWidgetModular(package),
    );
  }
}

Future<void> _installPackages(String directory) async {
  await install(['mockito'], true, directory: directory);
  await install(
    [
      'projectbasesnow: git: https://gitlab.com/snowman-labs/flutter-devs/project_base_snow.git'
    ],
    false,
    haveTwoLines: true,
    directory: directory,
  );
  await install(
    [
      'i18n_extension',
      "path_provider",
      "flutter_modular",
      "dio",
    ],
    false,
    directory: directory,
  );
}

void _createMainFile(
  String dirPath,
  String package, {
  bool useEnviroment = true,
  bool isModular = true,
}) {
  if (!useEnviroment) {
    createStaticFile(
      '$dirPath/main.dart',
      isModular
          ? templates.startMainModular(package)
          : templates.startMain(package),
    );
  } else {
    createStaticFile(
      '$dirPath/main_dev.dart',
      isModular
          ? templates.startMainDevModular(package)
          : templates.startMainDev(package),
    );
    createStaticFile(
      '$dirPath/main_production.dart',
      isModular
          ? templates.startMainProductionModular(package)
          : templates.startMainProduction(package),
    );
    createStaticFile(
      '$dirPath/main_qa.dart',
      isModular
          ? templates.startMainQaModular(package)
          : templates.startMainQa(package),
    );
  }
}

void _createStyleFiles() {
  createStaticFile(
    libPath('styles/app_bar_theme_app.dart'),
    templates.appBarThemeApp(),
  );
  createStaticFile(
    libPath('styles/app_color_scheme.dart'),
    templates.appColorSchemeApp(),
  );
  createStaticFile(
    libPath('styles/app_text_theme.dart'),
    templates.appTextThemeApp(),
  );
  createStaticFile(
    libPath('styles/app_theme_data.dart'),
    templates.appThemeDataApp(),
  );
}

void _createSharedFiles(String package, bool complete) {
  createStaticFile(
    libPath('shared/models/app_flavor_values.dart'),
    templates.appFlavorValues(),
  );
  createStaticFile(
    libPath('shared/widgets/custom_alert_dialog/confirm_alert_dialog.dart'),
    templates.customAlertDialog(),
  );
  createStaticFile(
    libPath('shared/widgets/custom_alert_dialog/types/confirm_dialog.dart'),
    templates.confirmDialogType(),
  );
  if (complete) {
    createStaticFile(
      libPath('shared/models/user_model.dart'),
      templates.userModel(),
    );
    createStaticFile(
      libPath('shared/auth/auth_controller.dart'),
      templates.authController(package),
    );
    createStaticFile(
      libPath('shared/auth/repositories/auth_repository_interface.dart'),
      templates.authRepoInterface(package),
    );
    createStaticFile(
      libPath('shared/auth/repositories/auth_repository.dart'),
      templates.authRepo(package),
    );
  }
}

void _createVsCodeSettings([String package = '', bool useFlavors = false]) {
  createStaticFile(
    package + '/.vscode/launch.json',
    useFlavors
        ? templates.launchSettingsVsCodeFlavor()
        : templates.launchSettingsVsCode(),
  );
}

void _createAndroidStudioSettings([
  String package = '',
  bool useFlavors = false,
]) {
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Dev___DEBUG.xml',
    useFlavors
        ? templates.settingsDevDebugAndroidStudioFlavor()
        : templates.settingsDevDebugAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Dev___PROFILE.xml',
    useFlavors
        ? templates.settingsDevProfileAndroidStudioFlavor()
        : templates.settingsDevProfileAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Dev___RELEASE.xml',
    useFlavors
        ? templates.settingsDevReleaseAndroidStudioFlavor()
        : templates.settingsDevReleaseAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Prod___DEBUG.xml',
    useFlavors
        ? templates.settingsProdDebugAndroidStudioFlavor()
        : templates.settingsProdDebugAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Prod___PROFILE.xml',
    useFlavors
        ? templates.settingsProdProfileAndroidStudioFlavor()
        : templates.settingsProdProfileAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_Prod___RELEASE.xml',
    useFlavors
        ? templates.settingsProdReleaseAndroidStudioFlavor()
        : templates.settingsProdReleaseAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_QA___DEBUG.xml',
    useFlavors
        ? templates.settingsQaDebugAndroidStudioFlavor()
        : templates.settingsQaDebugAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_QA___PROFILE.xml',
    useFlavors
        ? templates.settingsQaProfileAndroidStudioFlavor()
        : templates.settingsQaProfileAndroidStudio(),
  );
  createStaticFile(
    package + '/.idea/runConfigurations/Flutter_QA___RELEASE.xml',
    useFlavors
        ? templates.settingsQaReleaseAndroidStudioFlavor()
        : templates.settingsQaReleaseAndroidStudio(),
  );
}
