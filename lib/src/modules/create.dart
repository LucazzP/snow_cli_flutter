import 'dart:io';

import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart' show mainDirectory;
import 'package:tuple/tuple.dart';

Future<void> create(
    String projectName,
    String projectDescription,
    String projectOrg,
    bool isKotlin,
    bool isSwift,
    bool isAndroidX,
    String stateManagement,
    String provider,
    bool complete) {
  return startFlutterCreate(projectName, projectDescription, projectOrg,
      isKotlin, isSwift, isAndroidX, stateManagement, provider, complete);
}

Future<void> startFlutterCreate(
  String projectName,
  String projectDescription,
  String projectOrg,
  bool isKotlin,
  bool isSwift,
  bool isAndroidX,
  String stateManagement,
  String provider,
  bool complete,
) {
  mainDirectory = projectName + '/';

  var selectedProvider;
  var selectedState;

  if (provider == null) {
    selectedProvider = cleanOrModular(null, projectName);
  } else {
    selectedProvider = cleanOrModular(
        ['flutter_clean_architecture', 'flutter_modular'].indexOf(provider),
        projectName);
  }

  if (stateManagement == null) {
    selectedState = selectStateManagement(null, projectName);
  } else {
    selectedState =
        selectStateManagement(['mobx'].indexOf(stateManagement), projectName);
  }

  List<String> flutterArgs = createFlutterArgs(projectName, projectDescription,
      projectOrg, isKotlin, isSwift, isAndroidX);

  return Process.start("flutter", flutterArgs, runInShell: true)
      .then((process) async {
    stdout.addStream(process.stdout);
    stderr.addStream(process.stderr);
    final exitCode = await process.exitCode;
    if (exitCode == 0) {
      return startSlidyCreate(projectName, selectedProvider, selectedState, complete);
    }
  });
}

Future<void> startSlidyCreate(
    String projectName, Function selectedProvider, Function selectedState, bool complete) {
  return start(
      completeStart: complete,
      force: true,
      dir: Directory('$projectName/lib'),
      tuple: Tuple2(selectedProvider, selectedState));
}

List<String> createFlutterArgs(String projectName, String projectDescription,
    String projectOrg, bool isKotlin, bool isSwift, bool isAndroidX) {
  projectDescription = projectDescription == null
      ? "A new Flutter project. Created by Snowmanlabs with Flutter Snow Blower"
      : projectDescription;
  projectOrg = projectOrg == null ? "com.example" : projectOrg;

  var flutterArgs = ["create"];
  flutterArgs.add("--no-pub");

  if (isKotlin) {
    flutterArgs.add("-a");
    flutterArgs.add("kotlin");
  }

  if (isSwift) {
    flutterArgs.add("-i");
    flutterArgs.add("swift");
  }

  if (isAndroidX) {
    flutterArgs.add("--androidx");
  }

  flutterArgs.add("--project-name");
  flutterArgs.add("$projectName");

  if (projectDescription.isNotEmpty) {
    flutterArgs.add("--description");
    flutterArgs.add("'$projectDescription'");
  }

  if (projectOrg.isNotEmpty) {
    flutterArgs.add("--org");
    flutterArgs.add("$projectOrg");
  }

  flutterArgs.add(projectName);
  return flutterArgs;
}
