import 'dart:io';

import 'package:flutter_snow_blower/src/modules/update.dart';
import 'package:flutter_snow_blower/src/services/pub_service.dart';
import 'package:flutter_snow_blower/src/utils/pubspec.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart';
import 'package:flutter_snow_blower/src/utils/output_utils.dart' as output;

Future<void> install(List<String> packs, bool isDev, {bool haveTwoLines, String directory}) async {
  PubSpec spec = await getPubSpec(directory: directory == null ? null : Directory(directory));
  File yaml = File(directory == null ? "pubspec.yaml" : '$directory/pubspec.yaml');
  var node = yaml.readAsLinesSync();
  int indexDependency = node.indexWhere((t) => t.contains("dependencies:")) + 1;
  int indexDependencyDev =
      node.indexWhere((t) => t.contains("dev_dependencies:")) + 1;
  bool isAlter = false;
  haveTwoLines = haveTwoLines ?? false;
  var dependencies = isDev ? spec.devDependencies : spec.dependencies;

  for (String pack in packs) {
    String packName = "";
    String version = "";

    if (pack.contains(":")) {
      packName = pack.split(":")[0];
      version = pack.split(":").length > 1
          ? pack.split(":")[1] + ':' + pack.split(":").sublist(2).reduce((a, b) => a + ':' + b)
          : pack.split(":")[1];
    } else {
      packName = pack;
    }

    if (dependencies.containsKey(packName) && !haveTwoLines) {
      await update([packName], isDev);
      continue;
    }

    try {
      if (!haveTwoLines) {
        version = await PubService().getPackage(packName, version);
        node.insert(isDev ? indexDependencyDev : indexDependency,
            "  $packName: ^$version");
      } else if (!dependencies.containsKey(packName)) {
        node.insert(isDev ? indexDependencyDev : indexDependency,
            "  $packName: \n    $version");
      }
      output.success("$packName:$version Added in pubspec");
      isAlter = true;
    } catch (e) {
      output.error("Version or package not found");
    }
    if (isAlter) {
      yaml.writeAsStringSync(node.join("\n"));
    }
  }
}
