import 'dart:async';

import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class UpgradeCommand extends CommandBase {
  @override
  final name = 'upgrade';
  @override
  final description = 'Upgrade the Flutter Snow Blower CLI version';

  @override
  FutureOr<void> run() {
    upgrade();
  }
}
