import 'dart:async';

import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class CreateCommand extends CommandBase {
  @override
  final name = 'create';
  @override
  final description = 'Create a Flutter project with basic structure';
  @override
  final invocationSuffix = '<project name>';

  CreateCommand() {
    argParser.addFlag('complete',
        abbr: 'c',
        negatable: false,
        help:
            'Create a complete flutter project with a sample project with login structure');

    argParser.addOption('description',
        abbr: 'd',
        help:
            'The description to use for your new Flutter project. This string ends up in the pubspec.yaml file. (defaults to \"A new Flutter project. Created by Snowmanlabs with Flutter Snow Blower\")');

    argParser.addOption('org',
        abbr: 'o',
        help:
            'The organization responsible for your new Flutter project, in reverse domain name notation. This string is used in Java package names and as prefix in the iOS bundle identifier. (defaults to \"com.example\")');

    argParser.addOption('provider-system',
        abbr: 'p',
        allowed: ['flutter_clean_architecture', 'flutter_modular'],
        help: 'Create a flutter project using an specified provider system.');

    argParser.addOption('state-management',
        abbr: 'm',
        allowed: ['mobx'],
        help: 'Create a flutter project using an specified state management.');

    argParser.addFlag('kotlin',
        abbr: 'k', negatable: false, help: 'use kotlin in Android project');

    argParser.addFlag('swift',
        abbr: 's', negatable: false, help: 'use swift in ios project');

    argParser.addFlag('androidx',
        abbr: 'x', negatable: false, help: 'use androidx on android project');
  }

  @override
  FutureOr<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException(
          'project name not passed for a create command', usage);
    } else {
      await create(
        argResults.rest.first,
        argResults['description'],
        argResults['org'],
        argResults['kotlin'],
        argResults['swift'],
        argResults['androidx'],
        argResults['state-management'],
        argResults['provider-system'],
        argResults['complete'],
      );
    }
    super.run();
  }
}
