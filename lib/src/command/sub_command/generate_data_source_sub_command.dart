import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateDataSourceSubCommand extends CommandBase {
  final name = "data_source";
  final description = "Creates the Data Sources files";

  GenerateDataSourceSubCommand() {
    argParser.addFlag('notest',
        abbr: 'n', negatable: false, help: 'no create file test'
        );
  }

  Future<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException("value not passed for Data Source command", usage);
    } else {
      await Generate.dataSource(argResults.rest.first, haveTest: !argResults["notest"]);
    }
    super.run();
  }
}

class GenerateDataSourceAbbrSubCommand extends GenerateDataSourceSubCommand {
  final name = "ds";
}
