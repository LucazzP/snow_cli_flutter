import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateUseCaseSubCommand extends CommandBase {
  final name = "use_case";
  final description = "Creates a Use case";

  GenerateUseCaseSubCommand();

  Future<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException("value not passed for Use case command", usage);
    } else {
      await Generate.useCase(argResults.rest.first);
    }
    super.run();
  }
}

class GenerateUseCaseAbbrSubCommand extends GenerateUseCaseSubCommand {
  final name = "u";
}
