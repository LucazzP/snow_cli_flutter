import 'dart:async';

import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateModuleSubCommand extends CommandBase {
  @override
  final name = 'module';
  @override
  final description = 'Creates a module';

  GenerateModuleSubCommand() {
    argParser.addFlag('complete',
        abbr: 'c',
        negatable: false,
        help: 'Creates a module with Page, Controller and i18n files');
    argParser.addFlag('noroute',
        abbr: 'n',
        negatable: false,
        help: 'Creates a module withless named route');
  }

  @override
  Future<FutureOr<void>> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException('value not passed for a module command', usage);
    } else {
      await Generate.module(
        argResults.rest.first,
        argResults['complete'],
        argResults['noroute'],
      );
    }
    super.run();
  }
}

class GenerateModuleAbbrSubCommand extends GenerateModuleSubCommand {
  @override
  final name = 'm';
}
