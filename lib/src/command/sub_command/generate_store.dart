import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';

class GenerateStoreSubCommand extends CommandBase {
  final name = "store";
  final description = "Creates a Store";

  GenerateStoreSubCommand() {
    argParser.addFlag('notest',
        abbr: 'n', negatable: false, help: 'no create file test');
  }

  Future<void> run() async {
    if (argResults.rest.isEmpty) {
      throw UsageException("value not passed for a module command", usage);
    } else {
      await Generate.controller(
        argResults.rest.first,
        'store',
        haveTest: !argResults["notest"],
      );
    }
    super.run();
  }
}

class GenerateStoreAbbrSubCommand extends GenerateStoreSubCommand {
  final name = "ss";
}
