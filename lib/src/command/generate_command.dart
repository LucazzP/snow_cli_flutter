import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/command/sub_command/generate_controller.dart';
import 'package:flutter_snow_blower/src/command/sub_command/generate_data_source_sub_command.dart';
import 'package:flutter_snow_blower/src/command/sub_command/generate_entity_sub_command.dart';
import 'package:flutter_snow_blower/src/command/sub_command/generate_i18n.dart';
import 'package:flutter_snow_blower/src/command/sub_command/generate_store.dart';
import 'package:flutter_snow_blower/src/command/sub_command/generate_test_sub_command.dart';
import 'package:flutter_snow_blower/src/command/sub_command/generate_use_case_sub_command.dart';

class GenerateCommand extends CommandBase {
  @override
  final name = 'generate';
  @override
  final description =
      'Creates a module, page, widget or repository according to the option.';
  final abbr = 'g';

  GenerateCommand() {
    addSubcommand(GenerateModuleSubCommand());
    addSubcommand(GenerateModuleAbbrSubCommand());
    addSubcommand(GeneratePageSubCommand());
    addSubcommand(GeneratePageAbbrSubCommand());
    addSubcommand(GenerateWidgetSubCommand());
    addSubcommand(GenerateWidgetAbbrSubCommand());
    addSubcommand(GenerateStoreSubCommand());
    addSubcommand(GenerateControllerSubCommand());
    addSubcommand(GenerateControllerAbbrSubCommand());
    addSubcommand(GenerateStoreAbbrSubCommand());
    addSubcommand(GenerateRepositorySubCommand());
    addSubcommand(GenerateRepositoryAbbrSubCommand());
    addSubcommand(GenerateTestSubCommand());
    addSubcommand(GenerateTestAbbrSubCommand());
    addSubcommand(GenerateI18nSubCommand());
    addSubcommand(GenerateI18nAbbrSubCommand());
    addSubcommand(GenerateDataSourceSubCommand());
    addSubcommand(GenerateDataSourceAbbrSubCommand());
    addSubcommand(GenerateEntitySubCommand());
    addSubcommand(GenerateEntityAbbrSubCommand());
    addSubcommand(GenerateUseCaseSubCommand());
    addSubcommand(GenerateUseCaseAbbrSubCommand());
  }
}

class GenerateCommandAbbr extends GenerateCommand {
  @override
  final name = 'g';
}
