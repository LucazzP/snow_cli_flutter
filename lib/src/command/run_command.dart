import 'dart:async';

import 'package:args/command_runner.dart';
import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/modules/run.dart';

class RunCommand extends CommandBase {
  final name = "run";
  final description = "run scripts in pubspec.yaml";
  final invocationSuffix = "<project name>";

  @override
  FutureOr<void> run() {
    if (argResults.rest.isEmpty) {
      throw UsageException("script name not passed for a run command", usage);
    } else {
      return runCommand(argResults.rest);
    }
  }
}
