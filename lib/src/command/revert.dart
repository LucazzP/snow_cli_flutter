import 'dart:async';

import 'package:flutter_snow_blower/flutter_snow_blower.dart';
import 'package:flutter_snow_blower/src/utils/local_save_log.dart';

class RevertCommand extends CommandBase {
  @override
  final name = 'revert';
  bool argsLength(int n) => argResults.arguments.length > n;
  @override
  final description = 'Revert last command';

  @override
  FutureOr<void> run() async {
    LocalSaveLog().removeLastLog();
  }
}
