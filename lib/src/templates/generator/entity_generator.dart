import 'package:flutter_snow_blower/src/utils/object_generate.dart';
import 'package:flutter_snow_blower/src/utils/utils.dart';
import 'package:recase/recase.dart';

String entityGenerator(ObjectGenerate objectGenerate) {
  bool haveEquatable = objectGenerate.addicionalInfo;
  return '''
import 'package:flutter/foundation.dart';
${haveEquatable ? "import 'package:equatable/equatable.dart';" : ""}

@immutable
class ${objectGenerate.name}Entity ${haveEquatable ? "extends Equatable " : ""}{

  const ${objectGenerate.name}Entity();

  ${haveEquatable ? '''
  @override
  List<Object> get props => [name];
  ''' : ""}

  @override
  String toString() => '${objectGenerate.name}Info()'; // TODO Write toString of ${objectGenerate.name}

  ${!haveEquatable ? '''
  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ${objectGenerate.name}Entity; // TODO Write == operator of ${objectGenerate.name}
  }

  //@override
  //int get hashCode => property.hashCode; // TODO hashCode overrides of ${objectGenerate.name}
  ''' : ""}
}
''';
}

String entityMapperGenerator(ObjectGenerate objectGenerate) {
  final name = objectGenerate.name;
  final nameSnaked = ReCase(name).snakeCase;
  return '''
import 'dart:convert';

import 'package:${objectGenerate.packageName}/domain/entities/$nameSnaked/${nameSnaked}_entity.dart';

extension ${name}Mapper on ${name}Entity{

  ${name}Entity copyWith(
    // TODO Write copyWith of $name
  ) {
    return ${name}Entity(
      // TODO Write copyWith of $name
    );
  }

  Map<String, dynamic> toMap() {
    return {
      // TODO Write toMap of $name
    };
  }

  ${name}Entity fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return ${name}Entity(
      // TODO Write fromMap of $name
    );
  }

  String toJson() => json.encode(toMap());

  ${name}Entity fromJson(String source) => fromMap(json.decode(source));

}
''';
}
