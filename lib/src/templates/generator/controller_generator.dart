import 'package:recase/recase.dart';
import 'package:flutter_snow_blower/src/utils/object_generate.dart';

String mobxControllerGenerator(ObjectGenerate obj) => '''
import 'package:mobx/mobx.dart';

part '${ReCase(obj.name).snakeCase}_${obj.type}.g.dart';

class ${ReCase(obj.name + '_' + obj.type).pascalCase} = _${ReCase(obj.name + '_' + obj.type).pascalCase}Base with _\$${ReCase(obj.name + '_' + obj.type).pascalCase};

abstract class _${ReCase(obj.name + '_' + obj.type).pascalCase}Base with Store {
  // @observable
  // int value = 0;
  // 
  // @action
  // void increment() {
  //   value++;
  // }
}
  ''';
