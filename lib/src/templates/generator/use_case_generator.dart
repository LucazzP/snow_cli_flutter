import 'package:flutter_snow_blower/src/utils/object_generate.dart';

String useCaseGenerator(ObjectGenerate objectGenerate) {
  return '''
import 'package:${objectGenerate.packageName}/domain/usecases/base/base_future_use_case.dart';
import 'package:injectable/injectable.dart';

@injectable
class ${objectGenerate.name}Usecase implements BaseFutureUseCase<void, void> {
  const ${objectGenerate.name}Usecase();

  @override
  Future<void> call([void params]) {
    // TODO: implement call
    throw UnimplementedError();
  }
}
''';
}