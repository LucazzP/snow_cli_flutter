import 'package:flutter_snow_blower/src/utils/object_generate.dart';

String moduleGeneratorModular(ObjectGenerate obj) {
  var path = obj.pathModule.replaceFirst("lib/", "");
  var pkg = obj.packageName;

  var import =
      pkg.isNotEmpty ? "import 'package:$pkg/${path}_page.dart';" : '';
  var router = pkg.isNotEmpty
      ? "Router(Modular.initialRoute, child: (_, args) => ${obj.name}Page()),"
      : '';

  return '''
  import 'package:flutter_modular/flutter_modular.dart';
  ${import.replaceFirst('$pkg/$pkg', pkg)}
  class ${obj.name}Module extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<Router> get routers => [$router];

  static Inject get to => Inject<${obj.name}Module>.of();

}
  ''';
}

String moduleGeneratorModularNoRoute(ObjectGenerate obj) {
  var path = obj.pathModule.replaceFirst("lib/", "");
  var pkg = obj.packageName;
  var import =
      pkg.isNotEmpty ? "import 'package:$pkg/${path}_page.dart';" : '';

  return '''
  import 'package:flutter_modular/flutter_modular.dart';
  import 'package:flutter/material.dart';
  ${import.replaceFirst('$pkg/$pkg', pkg)}
  class ${obj.name}Module extends ModuleWidget {
  @override
  List<Bind> get binds => [];

  static Inject get to => Inject<${obj.name}Module>.of();

  @override  
  Widget get view => ${obj.name}Page();
}
  ''';
}
