String startAppModuleModular(String pkg) => '''
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:projectbasesnow/projectbasesnow.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';
import 'shared/models/app_flavor_values.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind<Dio>((i) => SnowDio(
              isMock: false /* FlavorConfig.isDevelopment */,
              baseUrl: FlavorConfig.values<AppFlavorValues>().baseUrl,
            )),
        Bind<LocalStorage>((i) => LocalStorageHive()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
  ''';

String startAppModuleModularComplete(String pkg) => '''
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:$pkg/src/modules/login/login_module.dart';
import 'package:$pkg/src/shared/auth/auth_controller.dart';
import 'package:$pkg/src/shared/auth/repositories/auth_repository.dart';
import 'package:$pkg/src/shared/auth/repositories/auth_repository_interface.dart';
import 'package:projectbasesnow/projectbasesnow.dart';

import 'app_widget.dart';
import 'modules/home/home_module.dart';
import 'modules/splash/splash_page.dart';
import 'shared/models/app_flavor_values.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind<Dio>((i) => SnowDio(
              isMock: false /* FlavorConfig.isDevelopment */,
              baseUrl: FlavorConfig.values<AppFlavorValues>().baseUrl,
            )),
        Bind<AuthRepository>((i) => AuthRepositoryDefault()),
        Bind<LocalStorage>((i) => LocalStorageHive()),
        Bind((i) => AuthController(), singleton: true),
      ];

  @override
  List<Router> get routers => [
        Router(
          Modular.initialRoute,
          child: (context, args) => SplashPage(),
          transition: TransitionType.noTransition,
        ),
        Router('/login',
            module: LoginModule(), transition: TransitionType.noTransition),
        Router('/home',
            module: HomeModule(), transition: TransitionType.noTransition),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
  ''';
