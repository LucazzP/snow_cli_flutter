String authRepoInterface(String package) => '''
import 'package:$package/src/shared/models/user_model.dart';

abstract class AuthRepository {
  Future<UserModel> getUser();
  Future<UserModel> loginGoogle();
  Future<UserModel> loginFacebook();
  Future<UserModel> loginEmailPassword(String email, String password);
  Future<UserModel> registerEmailPassword(String email, String password);
  Future<String> getToken();
  Future logout();
}
''';