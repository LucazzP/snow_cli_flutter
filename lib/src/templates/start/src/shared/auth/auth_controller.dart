String authController(String package) => '''
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:$package/src/shared/auth/repositories/auth_repository_interface.dart';
import 'package:$package/src/shared/models/user_model.dart';

part 'auth_controller.g.dart';

class AuthController = _AuthControllerBase with _\$AuthController;

enum AuthStatus { loading, login, logoff }

abstract class _AuthControllerBase with Store {
  final AuthRepository _authRepository = Modular.get();

  _AuthControllerBase() {
    _authRepository.getUser().then(setUser).catchError((e) {
      print('Error on AuthController: \$e');
    });
    autorun((_) {
      if (status == AuthStatus.login) {
        Modular.to.popUntil((route) => route.isFirst);
        Modular.to.pushReplacementNamed('/home');
      } else if (status == AuthStatus.logoff) {
        Modular.to.popUntil((route) => route.isFirst);
        Modular.to.pushReplacementNamed('/login');
      }
    });
  }

  @observable
  AuthStatus status = AuthStatus.loading;

  @observable
  UserModel user;

  @action
  void setUser(UserModel value) {
    user = value;
    status = user == null ? AuthStatus.logoff : AuthStatus.login;
  }

  Future<UserModel> loginWithEmail(String email, String password) async {
    final _user = await _authRepository.loginEmailPassword(email, password);
    setUser(_user);
    return _user;
  }

  Future<UserModel> registerWithEmail(String email, String password) async {
    final _user = await _authRepository.registerEmailPassword(email, password);
    setUser(_user);
    return _user;
  }

  @action
  Future<void> logout() async {
    setUser(await _authRepository.logout());
  }
}
''';