String startMainDev(String pkg) => '''
import 'package:projectbasesnow/projectbasesnow.dart';

import 'package:$pkg/src/app_module.dart';
import 'package:$pkg/src/shared/models/app_flavor_values.dart';

void main() {
  RunAppSnow(
    AppModule(),
    flavorValues: AppFlavorValues(
      baseUrl: 'https://jsonplaceholder.typicode.com',
    ),
    flavor: Flavor.dev,
    enableDevicePreview: false,
  );
}
  ''';

String startMainDevModular(String pkg) => '''
import 'package:flutter_modular/flutter_modular.dart';
import 'package:projectbasesnow/projectbasesnow.dart';

import 'package:$pkg/src/app_module.dart';
import 'package:$pkg/src/shared/models/app_flavor_values.dart';

void main() {
  RunAppSnow(
    ModularApp(
      module: AppModule(),
    ),
    flavorValues: AppFlavorValues(
      baseUrl: 'https://jsonplaceholder.typicode.com',
    ),
    flavor: Flavor.dev,
    enableDevicePreview: false,
  );
}
  ''';
