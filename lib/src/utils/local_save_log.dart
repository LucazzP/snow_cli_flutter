import 'dart:io';

import 'package:path/path.dart';
import 'package:flutter_snow_blower/src/utils/output_utils.dart' as output;

class LocalSaveLog {
  String _path;
  int _key;

  static final LocalSaveLog _instance = LocalSaveLog._internal();

  factory LocalSaveLog() {
    return _instance;
  }

  LocalSaveLog._internal() {
    _path = '.snow';
    _key = DateTime.now().millisecondsSinceEpoch;
  }

  void add(String value) {
    final file = File('$_path/$_key');
    if (!file.existsSync()) {
      file.createSync(recursive: true);
    }
    var body = file.readAsStringSync();
    file.writeAsStringSync('$body\n$value'.trim());
  }

  void removeLastLog() {
    _removeFiles(lastCreatedFiles());
  }

  File _getLastLogFile() {
    try {
      var dir = Directory(_path);
      if (dir.existsSync()) {
        var listInt =
            dir.listSync().map((e) => basename(e.path)).map(int.parse).toList();
        listInt.sort((a, b) => b.compareTo(a));
        final first = listInt.first;
        final file = File('$_path/$first');
        if (file.existsSync()) {
          return file;
        } else {
          throw Exception("No logs registred by snow blower");
        }
      }
    } catch (e) {
      print("No logs registred by snow blower");
    }
    return null;
  }

  List<File> lastCreatedFiles() {
    final lastLog = _getLastLogFile();
    final files = <File>[];
    for (var item in lastLog.readAsStringSync().split('\n')) {
      var arch = File(item);
      if (arch.existsSync()) {
        files.add(arch);
      }
    }
    return files;
  }

  void _removeFiles(List<File> files) {
    for (var item in files) {
      if (item.existsSync()) {
        item.deleteSync(recursive: true);
        _removeEmptyFolder(item.parent);
        output.warn('REMOVED: $item');
      }
    }
    _deleteLastLog();
  }

  void _deleteLastLog() {
    _getLastLogFile().deleteSync();
  }

  void _removeEmptyFolder(Directory dir) {
    if (dir.listSync().isEmpty) {
      dir.deleteSync();
      _removeEmptyFolder(dir.parent);
    }
  }
}
