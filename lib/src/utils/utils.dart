import 'dart:io';

import 'package:flutter_snow_blower/src/enums/architecture_enum.dart';
import 'package:flutter_snow_blower/src/modules/uninstall.dart';
import 'package:flutter_snow_blower/src/utils/pubspec.dart';
import 'package:yaml/yaml.dart';

String mainDirectory = '';
String _libPath;
Architecture _projectArchitecture;

String formatName(String name) {
  name = name
      .replaceAll("_", " ")
      .split(" ")
      .map((t) => t[0].toUpperCase() + t.substring(1))
      .join()
      .replaceFirst(".dart", "");
  return name;
}

Future<String> getNamePackage([Directory dir]) async {
  var yaml = await getPubSpec(directory: dir ?? Directory(mainDirectory));
  return yaml.name;
}

Architecture projectArchitecture([bool reset = false]) {
  if (!reset && _projectArchitecture != null) return _projectArchitecture;

  if (existsFile("data", reset) &&
      existsFile("device", reset) &&
      existsFile("domain", reset)) {
    _projectArchitecture = Architecture.cleanArchitecture;
  } else {
    _projectArchitecture = Architecture.modular;
  }
  return _projectArchitecture;
}

bool existsFile(String path, [bool reset = false]) {
  if(path.contains(".")){
    return File(libPath(path, reset)).existsSync();
  }
  return Directory(libPath(path, reset)).existsSync();
}

Future<bool> checkDependency(String dep) async {
  try {
    PubSpec yaml = await getPubSpec();
    return yaml.dependencies.containsKey(dep);
  } catch (e) {
    print(e);
    return false;
  }
}

Future<String> getVersion() async {
  File file =
      File(File.fromUri(Platform.script).parent.parent.path + "/pubspec.lock");
  var doc = loadYaml(file.readAsStringSync());
  return doc['packages']['snow']['version'].toString();
}

Future<PubSpec> getPubSpec({Directory directory}) async {
  var pubSpec = await PubSpec.load(directory ?? Directory("$mainDirectory"));
  return pubSpec;
}

Future removeAllPackages([String directory]) async {
  var pubSpec = await getPubSpec(
      directory: directory == null ? null : Directory(directory));
  var dep = pubSpec.dependencies.keys
      .map((f) => f.toString())
      .where((t) => t != "flutter")
      .toList();

  var devDep = pubSpec.devDependencies.keys
      .map((f) => f.toString())
      .where((t) => t != "flutter_test")
      .toList();

  await uninstall(dep, false, false, directory);
  await uninstall(devDep, true, false, directory);
}

bool checkParam(List<String> args, String param) {
  return args.contains(param);
}

String libPath(String path, [bool reset = false]) {
  if (reset || _libPath == null) {
    if (Directory("${mainDirectory}lib/src").existsSync()) {
      _libPath = "${mainDirectory}lib/src";
    } else if (Directory("${mainDirectory}lib/data").existsSync() &&
        Directory("${mainDirectory}lib/device").existsSync() &&
        Directory("${mainDirectory}lib/domain").existsSync()) {
      _libPath = "${mainDirectory}lib";
    } else if (Directory("${mainDirectory}lib/app").existsSync()) {
      _libPath = "${mainDirectory}lib/app";
    } else {
      _libPath = "${mainDirectory}lib/src";
    }
  }
  return _libPath + "/$path";
}

bool validateUrl(String url) {
  var urlPattern =
      r"(https?|http)://([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:‌​,.;]*)?";
  var match = RegExp(urlPattern, caseSensitive: false).firstMatch(url);
  return match != null ? true : false;
}
